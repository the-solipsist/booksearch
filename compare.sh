#!/bin/sh
booksearchpath=$HOME/booksearch
booklistspath=$booksearchpath/booklists
isbnpath=$booksearchpath/isbn
orderspath=$booksearchpath/orders

find -type f -iname "*.xlsx" -o -iname "*.xls" | tee $booklistspath/publists.txt
while read -r FILE; do
	in2csv $FILE | tee $FILE.csv
done <$booklistspath/publists.txt
grep -f $isbnpath/to-compare-google.txt booklists/*.csv | tee $orderspath/to-order-google.csv
grep -f $isbnpath/to-compare-openlibrary.txt booklists/*.csv | tee $orderspath/to-order-openlibrary.csv
cat $orderspath/to-order-*.csv | sort | uniq | tee $orderspath/to-order-all-unique.csv
find $isbnpath/google -size 0 -type f | awk -F/ '{print $NF}' | sed -e 's/isbns-//' -e 's/.txt//' | tee $isbnpath/no-isbn-google.txt
find $isbnpath/openlibrary -size 0 -type f | awk -F/ '{print $NF}' | sed -e 's/isbns-//' -e 's/.txt//' | tee $isbnpath/no-isbn-openlibrary.txt
cat $isbnpath/no-isbn*.txt | sort | uniq | tee $isbnpath/no-isbn-all-unique.txt
