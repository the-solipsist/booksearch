#!/bin/sh
booksearchpath=$HOME/booksearch
isbnpath=$booksearchpath/isbn
find $isbnpath/google -size 0 -type f | awk -F/ '{print $NF}' | sed -e 's/isbns-//' -e 's/.txt//' | tee $isbnpath/no-isbn_google.txt
find $isbnpath/openlibrary -size 0 -type f | awk -F/ '{print $NF}' | sed -e 's/isbns-//' -e 's/.txt//' | tee $isbnpath/no-isbn_openlibrary.txt

