
This set of scripts to help a book store.  It allows the book store to check if the books they want to order are available in the catalogues sent by publishers.

## Installation of Dependencies

To run these scripts, a set of software are needed on your device:

1. `bash` (and basic bash built-in tools like `grep`, `ls`, `while`, `tee`, `cat`, `uniq`, `sort`, `sed`, and `awk`)
2. `curl`
3. `jq`
4. `csvkit`
5. `git`

### Linux

If you're running Linux, then you need to install `curl` and `jq`, as the rest will already be available.

`sudo apt install curl jq csvkit`

### MacOS
If you're running MacOS, then you will need to install homebrew.  Open Terminal.app (or your favourite terminal emulator), and type:

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

After that, install the required tools:
`brew install curl jq csvkit git`

### Windows 10
If you're running Windows 10, follow the instructions on this page to get set up with the Windows Subsystem for Linux:
https://docs.microsoft.com/en-us/windows/wsl/install-win10

After getting that set up, run:

`sudo apt install curl jq csvkit`

## Running the scripts
1. First download the scripts from the Gitlab repository into your home folder:

`cd ~`
`git clone https://gitlab.com/the-solipsist/booksearch`

2. Rename `googleapi.key.sample` to `googleapi.key` and make sure it contains a correct API key.

3. Download the "searchstrings" tab from the Google Sheets file in csv format, and save it as `searchstrings.csv` in the "booklists" folder.  It should have two columns: "Title","Primary Author".  All spaces in the names are replaced with a "+".

4. Run the script.

`./booksearch/get-isbn-and-compare.sh`

## Files created

Six files will get created are:

In the "orders" sub-folder:

  a. to-order-all-unique.csv
  b. to-order-google.csv
  c. to-order-openlibrary.csv

In the "isbn" sub-folder:

  a. no-isbn-all-unique.txt
  b. no-isbn-google.txt
  c. no-isbn-openlibrary.txt

You probably only want "to-order-all-unique", which contains a list of all the matches between the books in "searchstrings.csv" and the publishers' lists.  You would also want to go through "no-isbn-all-unique.txt", which contains a list of books/authors for which ISBN numbers could not be found, and which you will need to search for manually.
