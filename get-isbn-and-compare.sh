#!/bin/sh
# Create a folder in your home or user folder, called "booksearch".
# Save "get-isbn.sh" and "compare.sh" and "get-isbn-and-compare.sh" in the "booksearch" folder.
# Within the "booksearch" folder, create sub-folders called "booklists", "isbn", and "orders".
# Within the "isbn" folder, create sub-folders called "google" and "openlibrary".
# Save all the publishers' XLSX files in "booklists". Ensure there are no spaces in the file name.
# Save search strings file as "searchstrings.csv" in the "booksearch" folder.
# Then run this by typing: "~/booksearch/get-isbn-and-compare.sh" in a terminal
booksearchpath=$HOME/booksearch
booklistspath=$booksearchpath/booklists
searchstrings=$booklistspath/searchstrings.csv

$booksearchpath/get-isbn.sh $searchstrings google # search using Google Books, saves ISBNs
$booksearchpath/get-isbn.sh $searchstrings openlibrary # search using Open Library, saves ISBNs
$booksearchpath/compare.sh # compares the ISBNs against publishers' lists and generates a csv of matches.
$booksearchpath/no-isbns.sh # creates lists of searches that did not manage to pull down any ISBNs.
