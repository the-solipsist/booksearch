#!/bin/sh
csv=$1
mode=$2
booksearchpath=$HOME/booksearch
isbnpath=$booksearchpath/isbn/
googlepath=$isbnpath/google
olpath=$isbnpath/openlibrary
. $booksearchpath/googleapi.key
while read -r LINE; do
	title=$(echo $LINE | csvcut -c1 -x)
	author=$(echo $LINE | csvcut -c2 -x)
	echo "$title   |  $author"
	if [ "$author" = "" ]
		then
		if [ "$mode" = "google" ]
			then
		        curl --silent "https://www.googleapis.com/books/v1/volumes?key=$key&q=intitle:\"$title\"" | jq -r '.items[]?.volumeInfo.industryIdentifiers[]?.identifier' | tee -a $googlepath/isbns-$title.txt
		fi
		if [ "$mode" = "openlibrary" ]
			then
			curl --silent "https://openlibrary.org/search.json?title=\"$title\"" | jq -r '.docs[]?.isbn[]?' | tee -a $olpath/isbns-$title.txt
		fi
	fi
	if [ "$title" = "" ]
		then
		if [ "$mode" = "google" ]
			then
		        curl --silent "https://www.googleapis.com/books/v1/volumes?key=$key&q=intitle:\"$title\"+inauthor:\"$author\"" | jq -r '.items[]?.volumeInfo.industryIdentifiers[]?.identifier' | tee -a $googlepath/isbns-$author.txt
		fi
		if [ "$mode" = "openlibrary" ]
			then
			curl --silent "https://openlibrary.org/search.json?author=\"$author\"" | jq -r '.docs[]?.isbn[]?' | tee -a $olpath/isbns-$author.txt
		fi
	fi
	if [ "$author" != "" ]
		then
		if [ "$mode" = "google" ]
			then
			curl --silent "https://www.googleapis.com/books/v1/volumes?key=$key&q=intitle:\"$title\"+inauthor:\"$author\"" | jq -r '.items[]?.volumeInfo.industryIdentifiers[]?.identifier' | tee -a $googlepath/isbns-$author.txt
		fi
		if [ "$mode" = "openlibrary" ]
			then
			curl --silent "https://openlibrary.org/search.json?author=\"$author\"&title=\"$title\"" | jq -r '.docs[]?.isbn[]?' | tee -a $olpath/isbns-$author.txt
		fi
	fi
done < $csv
if [ "$mode" = "google" ]
	then
	cat $googlepath/isbns-*.txt | tee $isbnpath/to-compare-google.txt
fi
if [ "$mode" = "openlibrary" ]
	then
	cat $olpath/isbns-*.txt | tee $isbnpath/to-compare-openlibrary.txt
fi
